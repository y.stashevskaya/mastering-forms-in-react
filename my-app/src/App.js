import './App.css';
import './reset_stylesheet.css';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';


function App() {
  return (
   <UserForm />
  );
}

const initialValues = {
  firstName: '',
  lastName: '',
  age: '',
  employed: false,
  color: '',
  sauces: [],
  stooge: 'Larry',
  notes: '',
  notesBox: '',
}

function isFormEmpty(values) {
  return values === initialValues
}

function formDescription(values) {
  var properties = ['stooge', 'employed']
  if (values.firstName.length > 0) {
    properties.push('firstName')
  }
  if (values.lastName.length > 0) {
    properties.push('lastName')
  }
  if (values.age.length > 0) {
    properties.push('firagestName')
  }
  if (values.color.length > 0) {
    properties.push('color')
  }
  if (values.sauces.length > 0) {
    properties.push('sauces')
  }
  if (values.notes.length > 0) {
    properties.push('notes')
  }
  return JSON.stringify(values, properties, 2)
}

function UserForm() {
  return (
    <div className = 'userForm'>
    <Formik
      initialValues = {initialValues}
      validationSchema = {Yup.object({
        firstName: Yup.string()
          .matches(/^[A-Za-z ]*$/),
        lastName: Yup.string()
          .matches(/^[A-Za-z ]*$/),
        age: Yup.number(),
        notes: Yup.string()
          .max(100)
      })}
      onSubmit = {(values) => {
        alert(formDescription(values));
      }}
    >
      {({ values, errors }) => (
      <Form>
        <div className='firstName'>
          <label htmlFor="firstName">First Name</label>
          <Field className={`${errors.firstName ? "errBorder" : ""}`} id="firstName" name="firstName" placeholder="First Name" />
        </div>
        <div className='lastName'>
          <label htmlFor="lastName" >Last Name</label>
          <Field className={`${errors.lastName ? "errBorder" : ""}`} id="lastName" name="lastName" placeholder="Last Name" />
        </div>
        <div className='age'>
          <label htmlFor="age">Age</label>
          <Field className={`${errors.age ? "errBorder" : ""}`} id="age" name="age" placeholder="Age" />
        </div>
        <label style={{display: 'flex', paddingBottom: 10, paddingTop: 10}}>
          Employed
            <Field type="checkbox" name="employed" style={{marginLeft: 240}}/>
        </label>
        <div className='favColor'>
          <label htmlFor="favColor" style={{display: 'flex'}}>Favorite Color</label>
          <Field as="select" name="color">
            <option value=""></option>
            <option value="#ff0000">Red</option>
            <option value="#00ff00">Green</option>
            <option value="#0000ff">Blue</option>
          </Field>
        </div>
          <div className="sauces" >Sauces
            <div className="sauceGroup" role="group" aria-labelledby="sauces">
              <label className='inputGroup'>
                <Field type="checkbox" name="sauces" value="Ketchup" />
                Ketchup
              </label>
              <label className='inputGroup'>
                <Field type="checkbox" name="sauces" value="Mustard" />
                Mustard
              </label>
              <label className='inputGroup'>
                <Field type="checkbox" name="sauces" value="Mayonnaise" />
                Mayonnaise
              </label>
              <label className='inputGroup'>
                <Field type="checkbox" name="sauces" value="Guacamole" />
                Guacamole
              </label>
            </div>
          </div>
          <div className="stooge">Best Stooge
            <div className="stoogeGroup" role="group" aria-labelledby="my-radio-group">
              <label className='inputGroup'>
                <Field type="radio" name="stooge" value="Larry" />
                Larry
              </label>
              <label className='inputGroup'>
                <Field type="radio" name="stooge" value="Moe" />
                Moe
              </label>
              <label className='inputGroup'>
                <Field type="radio" name="stooge" value="Curly" />
                Curly
              </label>
            </div>
          </div>
          <div className='notes'>
            <label htmlFor="notes">Notes</label>
            <Field className={`${errors.notes ? "errBorder" : ""}`} as="textarea" name="notes" placeholder="Notes"/>
          </div>
          <div className='buttons'>
            <button type="submit" style={{color: 'white', backgroundColor: '#008CBA', borderColor: 'lightBlue'}} disabled={isFormEmpty(values)}>Submit</button>
            <button type="reset" style={{color: 'grey'}} disabled={isFormEmpty(values)}>Reset</button>
          </div>
          <div className='outputBox'>
              <pre>{formDescription(values)}</pre>
          </div>
        </Form>
      )}
    </Formik>
    </div>
  )
}

export default App;